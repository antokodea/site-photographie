SET SCHEMA 'site_pers';

CREATE TABLE _collection (
    nom             VARCHAR PRIMARY KEY,
    date            VARCHAR
);

CREATE TABLE _image (

    nom             VARCHAR,
    collection      VARCHAR,
    chemin          VARCHAR,

    CONSTRAINT collec_img_fk FOREIGN KEY (collection)
        REFERENCES _collection(nom)

);

DROP TABLE _image;
DROP TABLE _collection;

INSERT INTO _collection VALUES ('Photo Trégastel', '11 novembre 2022');

INSERT INTO _image VALUES ('pres', 'Photo Trégastel', 'ed90q4.jpg');

INSERT INTO _image VALUES ('pres', 'Photo Trégastel', 'ed90q4.jpg');
INSERT INTO _image VALUES ('Face au crépuscule', 'Photo Trégastel', '9faaoe.jpg');
INSERT INTO _image VALUES ('"Pourquoi y a du lisse ?"', 'Photo Trégastel', 'egsblp.jpg');
INSERT INTO _image VALUES ('Fracasse-roche', 'Photo Trégastel', 'tk12qo.jpg');
INSERT INTO _image VALUES ('En avant !', 'Photo Trégastel', '5khe89.jpg');
INSERT INTO _image VALUES ('Le Trône de Pierre', 'Photo Trégastel', 'q63v3x.jpg');
INSERT INTO _image VALUES ('Prendre son temps', 'Photo Trégastel', 'ed90q4.jpg');
INSERT INTO _image VALUES ('Calogero vibes', 'Photo Trégastel', 'd2wqiq.jpg"');
INSERT INTO _image VALUES ('Prendre le large', 'Photo Trégastel', 'vd59aa.jpg');
INSERT INTO _image VALUES ('Le Soleil se couche sur nos peines', 'Photo Trégastel', '1oyo8d.jpg');
