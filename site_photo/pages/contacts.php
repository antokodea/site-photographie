<!DOCTYPE html>
<html lang="fr">
<head>
    <link rel="stylesheet" href="style.css"/>
    <title>Antoine's Equipment</title>
</head>
<body>

    <?php 

        include("header/header.php");

    ?>

    <main class="pagecontact">

        <p class="contact">Mes Contacts</p>

        <ul class="listcontact">

            <li class="listcontact__data">
                <img class="listcontact__data__img" src="../ressources/logo/instagram.svg"></img>
                <p class="listcontact__data__txt">antx_lgn</p>
            </li>

            <li class="listcontact__data">
                <img class="listcontact__data__img" src="../ressources/logo/mail.svg"></img>
                <p class="listcontact__data__txt"><a href="mailto:antoine.lagane01@gmail.com">antoine.lagane01@gmail.com</a></p>
            </li>


        </ul>

    </main>
    
    <?php 
    
        include('footer/footer.php');

    ?>