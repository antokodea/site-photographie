<!DOCTYPE html>
<html lang="fr">
<head>
    <link rel="stylesheet" href="style.css"/>
    <title>Antoine's Equipment</title>
</head>
<body>

    <?php 

        include("header/header.php");

    ?>

    <main>

        <p class="appareil">Mon Appareil Photo</p>

        <div class="materiel">

            <img src="../ressources/logo/appareil.jpg" class="materiel__img"></img>

            <article class="materiel__text">

                <p class="materiel__text__appareil">Nikon D3500</p>

                <p class="materiel__text__class">Objectifs :</p>
                <ul class="materiel__text__objectifs">

                    <li class="materiel__text__objectif__line">AF-P Nikkor 18-55mm f/3,5-5,6</li>
                    <li class="materiel__text__objectif__line__m">AF-S Nikkor 55-300m f/4,5-5,6</li>
                    <li class="materiel__text__objectif__line__m">AF-S Nikkor 50mm f/1,8</li>

                </ul>

            </article>

        </div>

    </main>
    
    <?php 
    
        include('footer/footer.php');

    ?>