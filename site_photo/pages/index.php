<!DOCTYPE html>
<html lang="fr">
<head>
    <link rel="stylesheet" href="style.css"/>
    <title>Antoine's Photos</title>
</head>
<body>

    <?php 

        include("header/header.php");

    ?>

    <main class="accueil_site">

        <h1 class="welcome">Bienvenue sur mon site !</h1>

        <article class="presentation">

            <p class="presentation__p">

                Je m'appelle Antoine et je suis un grand passionné de photographie depuis maintenant un bon moment.<br>
                Je vais répertorier sur ce site les photos que je prends avec mon appareil photo et mon téléphone.<br>

            </p>

        </article>

        <h2 class="presentation__p__visio">Bon visionnage !</h2>

        <h2 class="annonce_collec">Mes Collections</h2>

        <section class="lastadd"> 

            <?php

                include('../ressources/database/connect_params.php');

                $dbh = new PDO("$driver:host=$server;dbname=$dbname", $user, $pass);

                $dbh -> query("SET SCHEMA 'site_pers'");
            
                foreach($dbh->query('SELECT * from _collection', PDO::FETCH_ASSOC) as $row) {

                    echo '<figure class="lastadd__element">';

                    $res = $dbh->query("SELECT chemin FROM _image i INNER JOIN _collection c ON i.collection = c.nom WHERE i.collection = '" . $row['nom']."' AND i.nom = 'pres'") -> fetch();

                    echo '<a href="page_collection.php?nom='.$row['nom'].'"> <img src="https://i.goopics.net/'.$res[0].'" class="lastadd__element__img"></img> </a>';

                    echo '<figcaption class="lastadd__element__title">'.$row['nom'].'</figcaption>';

                    echo '</figure>';

                }
            
            ?>            

        </section>

    </main>
    
    <?php 
    
        include('footer/footer.php');
    
    ?>