    <footer>

        <ul class="footer__menu">

            <li><a class="footer__menu__line" href="materiel.php">Mon matériel</a></li>
            <li><a class="footer__menu__line"href="contacts.php">Contacts</a></li>

        </ul>

        
        <div class="footer__citation">

            <q class="footer__citation__quote">Prendre quelqu’un en photo, c’est un peu comme le rencontrer dans un train</q>
            <p class="footer__citation__author">Martine Franck</p>

        </div>

    </footer>

</body>
</html>