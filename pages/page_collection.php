<!DOCTYPE html>
<html lang="fr">
<head>
    <link rel="stylesheet" href="style.css"/>
    <?php

        echo "<title>Photos - ".$_GET['nom']."</title>";

    ?>
</head>
<body>

    <?php 

        include("header/header.php");

    ?>

    <main class="accueil_site">

        <section class="collection"> 

            <?php

                include('../ressources/database/connect_params.php');

                $dbh = new PDO("$driver:host=$server;dbname=$dbname", $user, $pass);

                $dbh -> query("SET SCHEMA 'site_pers'");
            
                foreach($dbh->query("SELECT * from _image WHERE collection='".$_GET['nom']."' AND nom <> 'pres'", PDO::FETCH_ASSOC) as $row) {

                    echo '<figure class="collection__photo">';

                    echo '<img src="https://i.goopics.net/'.$row['chemin'].'" class="collection__photo__img">';

                    echo '<figcaption class="collection__photo__name">'.$row['nom'].'</figcaption>';

                    echo '</figure>';

                }
            
            ?>            

        </section>

    </main>
    
    <?php 
    
        include('footer/footer.php');

    ?>